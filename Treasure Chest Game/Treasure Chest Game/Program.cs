﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treasure_Chest_Game
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("JUEGO DE LOS COFRES DEL TESORO");
            Console.WriteLine("Elige uno de los cofres en cada habitación hasta llegar al final.");
        INICIO:

            var rnd = new Random();
            char Cofre1 = (char)rnd.Next(65, 67);
            char Cofre2 = (char)rnd.Next(65, 67);
            char Cofre3 = (char)rnd.Next(65, 67);
            char Cofre4 = (char)rnd.Next(65, 67);
            char Cofre5 = (char)rnd.Next(65, 67);
            //Console.WriteLine("SPOILER: {0},{1},{2},{3},{4}"
            //    , Cofre1, Cofre2, Cofre3, Cofre4, Cofre5);

            Console.WriteLine(" ------------------------------------------------------ ");
            Console.WriteLine("|                    HABITACIÓN 1                      |");
            Console.WriteLine("|   █████                                      █████   |");
            Console.WriteLine("|  ██▀ ▀██                                    ██▀ ▀██  |");
            Console.WriteLine("|  ███████                                    ███████  |");
            Console.WriteLine("|  COFRE A                                    COFRE B  |");
            Console.WriteLine(" ------------------------------------------------------ ");
            Console.Write("Escribe A o B para abrir un cofre: ");
            string Cofre = Console.ReadLine().ToUpper();
            if(Cofre == Cofre1.ToString())
            {
                Console.WriteLine("Bien. Vamos a la siguiente habitación.\n\n");
            }
            else
            {
                Console.WriteLine("Uhhh. Cofre incorrecto. Toma las 5 rupias y vuelve al inicio.\n\n\n\n");
                goto INICIO;
            }

            Console.WriteLine(" ------------------------------------------------------ ");
            Console.WriteLine("|                    HABITACIÓN 2                      |");
            Console.WriteLine("|   █████                                      █████   |");
            Console.WriteLine("|  ██▀ ▀██                                    ██▀ ▀██  |");
            Console.WriteLine("|  ███████                                    ███████  |");
            Console.WriteLine("|  COFRE A                                    COFRE B  |");
            Console.WriteLine(" ------------------------------------------------------ ");
            Console.Write("Escribe A o B para abrir un cofre: ");
            Cofre = Console.ReadLine().ToUpper();
            if (Cofre == Cofre2.ToString())
            {
                Console.WriteLine("Bien. Vamos a la siguiente habitación.\n\n");
            }
            else
            {
                Console.WriteLine("Uhhh. Cofre incorrecto. Toma las 5 rupias y vuelve al inicio.\n\n\n\n");
                goto INICIO;
            }

            Console.WriteLine(" ------------------------------------------------------ ");
            Console.WriteLine("|                    HABITACIÓN 3                      |");
            Console.WriteLine("|   █████                                      █████   |");
            Console.WriteLine("|  ██▀ ▀██                                    ██▀ ▀██  |");
            Console.WriteLine("|  ███████                                    ███████  |");
            Console.WriteLine("|  COFRE A                                    COFRE B  |");
            Console.WriteLine(" ------------------------------------------------------ ");
            Console.Write("Escribe A o B para abrir un cofre: ");
            Cofre = Console.ReadLine().ToUpper();
            if (Cofre == Cofre3.ToString())
            {
                Console.WriteLine("Bien. Vamos a la siguiente habitación.\n\n");
            }
            else
            {
                Console.WriteLine("Uhhh. Cofre incorrecto. Toma las 5 rupias y vuelve al inicio.\n\n\n\n");
                goto INICIO;
            }

            Console.WriteLine(" ------------------------------------------------------ ");
            Console.WriteLine("|                    HABITACIÓN 4                      |");
            Console.WriteLine("|   █████                                      █████   |");
            Console.WriteLine("|  ██▀ ▀██                                    ██▀ ▀██  |");
            Console.WriteLine("|  ███████                                    ███████  |");
            Console.WriteLine("|  COFRE A                                    COFRE B  |");
            Console.WriteLine(" ------------------------------------------------------ ");
            Console.Write("Escribe A o B para abrir un cofre: ");
            Cofre = Console.ReadLine().ToUpper();
            if (Cofre == Cofre4.ToString())
            {
                Console.WriteLine("Bien. Vamos a la siguiente habitación.\n\n");
            }
            else
            {
                Console.WriteLine("Uhhh. Cofre incorrecto. Toma las 5 rupias y vuelve al inicio.\n\n\n\n");
                goto INICIO;
            }

            Console.WriteLine(" ------------------------------------------------------ ");
            Console.WriteLine("|                    HABITACIÓN 5                      |");
            Console.WriteLine("|   █████                                      █████   |");
            Console.WriteLine("|  ██▀ ▀██                                    ██▀ ▀██  |");
            Console.WriteLine("|  ███████                                    ███████  |");
            Console.WriteLine("|  COFRE A                                    COFRE B  |");
            Console.WriteLine(" ------------------------------------------------------ ");
            Console.Write("Escribe A o B para abrir un cofre: ");
            Cofre = Console.ReadLine().ToUpper();
            if (Cofre == Cofre5.ToString())
            {
                Console.WriteLine("¡¡FELICIDADES!!, HAS LLEGADO AL FINAL.");
                Console.WriteLine("Toma la pieza de contenedor de corazón y hasta la próxima.\n\n");
            }
            else
            {
                Console.WriteLine("Uhhh. Cofre incorrecto. Toma las 5 rupias y vuelve al inicio.\n\n\n\n");
                goto INICIO;
            }

            Console.ReadKey();
        }
    }
}
